package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class IServiceDataSourceImp implements IServiceDataSource {
    URL sourceUrl;
    BufferedReader bufferReader;
    private List<Tabletop> games;
    private Context context;
    String urlFullText;
    String urlText;

    public IServiceDataSourceImp(Context context){
        this.context = context;
    }

    @Override
    public List<Tabletop> obtainItems() throws BaseDataItemsException {
        games = new ArrayList<Tabletop>();
        try {
            getFileFromWeb asyn = new getFileFromWeb();
            // asyn.execute(); // NO ENTIENDO PORQUE NO FUNCIONA, si asi dice Stack Overflow que deberia ser
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
        return games;
    }

    public class getFileFromWeb extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            HttpURLConnection connection = null;
            bufferReader = null;

            try {
                sourceUrl = new URL("https://bitbucket.org/lyonv/ci0161_i2020_examenii/raw/996c22731408d9123feac58627318a7859e82367/Tabletop27.json");

                bufferReader = new BufferedReader(new InputStreamReader(sourceUrl.openStream()));

                while ((urlText = bufferReader.readLine()) != null) {

                    urlFullText += urlText;
                }
                bufferReader.close();

                JSONObject obj = new JSONObject();
                try {
                    obj = new JSONObject(urlFullText);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JSONArray gamesArray = obj.getJSONArray("miTabletop");

                for (int i = 0; i < gamesArray.length(); i++) {
                    JSONObject gameDetail = gamesArray.getJSONObject(i);
                    Tabletop t = new Tabletop(
                            gameDetail.getString("identificacion"),
                            gameDetail.getString("nombre"),
                            Integer.parseInt(gameDetail.getString("year")),
                            gameDetail.getString("publisher"));
                    games.add(t);
                }
            } catch (IOException malformedURLException) {
                malformedURLException.printStackTrace();
                urlFullText = malformedURLException.toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void finalTextHolder) {
            super.onPostExecute(finalTextHolder);
        }
    }
}
