package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;

import java.util.List;

public interface ItemsRepository {
    List<Tabletop> obtainItems(Context context) throws CantRetrieveItemsException;
}
