package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;

import java.util.List;

public class MainActivityPresenterImpl implements MainActivityPresenter, TabletopInteractor.OnFinishedListener{
    private MainActivityView mMainActivityView;
    private TabletopInteractor mTabletopInteractor;
    private Context context;

    public MainActivityPresenterImpl(MainActivityView mainActivityView, Context context) {

        this.mMainActivityView = mainActivityView;
        this.context = context;

        this.mTabletopInteractor = new TabletopInteractorImpl();
    }

    @Override public void onResume() {
        if (mMainActivityView != null) {
            mMainActivityView.showProgress();
        }
        mTabletopInteractor.getItems(this, context);
    }

    @Override public void onItemClicked(int position) {
        if (mMainActivityView != null) {
            mMainActivityView.startDetailsActivity(position);
        }
    }

    @Override public void onDestroy() {
        mMainActivityView = null;
    }

    @Override
    public void onFinished(List<Tabletop> games) {

        if (mMainActivityView != null) {

            mMainActivityView.setItems(games);
            mMainActivityView.hideProgress();
        }
    }

    public MainActivityView getMainView() {
        return mMainActivityView;
    }
}
