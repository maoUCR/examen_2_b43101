package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainActivityView, AdapterView.OnItemClickListener  {

    private ListView mListView;
    private ProgressBar mProgressBar;
    private MainActivityPresenter mMainActivityPresenter;
    LazyAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mListView = (ListView) findViewById(R.id.custom_list_item);
        mListView.setOnItemClickListener(this);

        mProgressBar = (ProgressBar) findViewById(R.id.progress);
        cargarInfo();

        Context newContext = getApplicationContext();

        mMainActivityPresenter = new MainActivityPresenterImpl(this, newContext);
    }

    public void startDetailsActivity(int position)
    {
        Tabletop tabletop = (Tabletop) adapter.getItem(position);
        Intent detailsIntent = new Intent(this, TabletopDetailsActivity.class);
        detailsIntent.putExtra("TABLETOP", tabletop);
        startActivity(detailsIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMainActivityPresenter.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        mMainActivityPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mListView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setItems(List<Tabletop> items) {
        adapter = new LazyAdapterImpl(this, items);

        mListView.setAdapter((ListAdapter) adapter);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        mMainActivityPresenter.onItemClicked(position);
    }

    public void cargarInfo(){
        MainActivityPresenterImpl implementation = new MainActivityPresenterImpl(this, getApplicationContext());
        implementation.onResume();
    }
}