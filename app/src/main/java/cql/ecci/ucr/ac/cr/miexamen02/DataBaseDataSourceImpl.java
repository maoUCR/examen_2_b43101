package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class DataBaseDataSourceImpl implements DataBaseDataSource {
    private List<Tabletop> games;
    private Context context;

    public DataBaseDataSourceImpl(Context context) {

        this.context = context;
    }

    @Override
    public List<Tabletop> obtainItems() throws BaseDataItemsException {

        games = new ArrayList<Tabletop>();
        try {
            getTabletops();
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
        return games;
    }

    private void getTabletops() {
        DataBaseHelperImpl dataBaseHelperImpl = new DataBaseHelperImpl(context);
        SQLiteDatabase db = dataBaseHelperImpl.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from TABLETOP",null);

        if (cursor.moveToFirst()) {

            while (!cursor.isAfterLast()) {

                this.games.add(new Tabletop(cursor.getString(0),
                                            cursor.getString(1),
                                            cursor.getInt(2),
                                            cursor.getString(3)));
                        cursor.moveToNext();
            }
        }
    }
}