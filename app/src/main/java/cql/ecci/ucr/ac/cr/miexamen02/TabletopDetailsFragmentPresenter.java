package cql.ecci.ucr.ac.cr.miexamen02;

public interface TabletopDetailsFragmentPresenter {
    public void onResume();

    public void onDestroy();
}
