package cql.ecci.ucr.ac.cr.miexamen02;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class TabletopDetailsActivity extends AppCompatActivity implements TabletopDetailsActivityView {

    public static final int fragmentContainerId = R.id.details_fragments_container;

    private TabletopDetailsActivityPresenterImpl detailsActivityPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabletop_details);

        detailsActivityPresenter = new TabletopDetailsActivityPresenterImpl(this, getApplicationContext());

        if (savedInstanceState == null) {

            getSupportFragmentManager().beginTransaction()
                    .add(fragmentContainerId, TabletopDetailsFragment.newInstance())
                    .commitNow();
        }
    }

    @Override
    protected void onResume() {
        detailsActivityPresenter.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        detailsActivityPresenter.onDestroy();
        super.onDestroy();
    }
}