package cql.ecci.ucr.ac.cr.miexamen02;

public interface MainActivityPresenter {
    void onResume();

    void onItemClicked(int position);

    void onDestroy();
}
