package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;

import java.util.List;

public class ItemsRepositoryImpl implements ItemsRepository{
    private IServiceDataSource source;

    @Override
    public List<Tabletop> obtainItems(Context context) throws CantRetrieveItemsException {
        List<Tabletop> games = null;

        try {
            // Sino tambien podemos llamar la data de la base de datos local, aka new DataBaseDataSourceImpl(context);
            source = new IServiceDataSourceImp(context);
            games = source.obtainItems();
        } catch (Exception e) {
            throw new CantRetrieveItemsException(e.getMessage());
        }

        return games;
    }
}
