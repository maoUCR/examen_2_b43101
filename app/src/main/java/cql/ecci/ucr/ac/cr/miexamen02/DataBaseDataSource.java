package cql.ecci.ucr.ac.cr.miexamen02;

import java.util.List;

public interface DataBaseDataSource {
    List<Tabletop> obtainItems() throws BaseDataItemsException;
}

