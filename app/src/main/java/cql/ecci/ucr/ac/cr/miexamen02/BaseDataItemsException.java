package cql.ecci.ucr.ac.cr.miexamen02;

public class BaseDataItemsException extends Exception {
    public BaseDataItemsException(String msg) {
        super(msg);
    }
}
