package cql.ecci.ucr.ac.cr.miexamen02;

import android.provider.BaseColumns;

public final class DataBaseContract {

    private DataBaseContract() { }
        public static class DataBaseEntry implements BaseColumns {

            // Clase Tabletop
            public static final String TABLE_NAME_TABLETOP = "Tabletop";

            public static final String COLUMN_NAME_IDENTIFICACION = "identificacion";
            public static final String COLUMN_NAME_NOMBRE = "nombre";
            public static final String COLUMN_NAME_YEAR = "year";
            public static final String COLUMN_NAME_PUBLISHER = "publisher";
        }

        private static final String TEXT_TYPE = " TEXT";
        private static final String INTEGER_TYPE = " INTEGER";
        private static final String COMMA_SEP = ",";

        public static final String SQL_CREATE_TABLETOP =
                "CREATE TABLE " + DataBaseEntry.TABLE_NAME_TABLETOP + " (" +
                        DataBaseEntry.COLUMN_NAME_IDENTIFICACION + TEXT_TYPE + "PRIMARY KEY," +
                        DataBaseEntry.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMMA_SEP +
                        DataBaseEntry.COLUMN_NAME_YEAR + INTEGER_TYPE + COMMA_SEP +
                        DataBaseEntry.COLUMN_NAME_PUBLISHER + TEXT_TYPE +  " )";

        public static final String SQL_DELETE_TABLETOP =
                "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_TABLETOP;

}

