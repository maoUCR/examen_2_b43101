package cql.ecci.ucr.ac.cr.miexamen02;

import java.util.List;

public interface IServiceDataSource {
    List<Tabletop> obtainItems() throws BaseDataItemsException;
}
