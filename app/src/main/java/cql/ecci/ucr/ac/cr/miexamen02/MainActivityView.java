package cql.ecci.ucr.ac.cr.miexamen02;

import java.util.List;

public interface MainActivityView {

    void showProgress();

    void hideProgress();

    void setItems(List<Tabletop> items);

    void showMessage(String message);

    public void startDetailsActivity(int position);
}
