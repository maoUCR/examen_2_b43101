package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;

public class TabletopDetailsActivityPresenterImpl implements TabletopDetailsActivityPresenter {
    TabletopDetailsActivityView detailsActivity;
    Context context;

    TabletopDetailsActivityPresenterImpl(TabletopDetailsActivityView detailsActivityView, Context context) {
        this.detailsActivity = detailsActivityView;
    }

    @Override
    public void onResume() {}

    @Override
    public void onDestroy() {

        this.detailsActivity = null;
    }

}
