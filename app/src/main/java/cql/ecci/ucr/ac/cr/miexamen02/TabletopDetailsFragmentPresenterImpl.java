package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;

public class TabletopDetailsFragmentPresenterImpl implements TabletopDetailsFragmentPresenter
{
    private TabletopDetailsFragmentView detailsFragment;
    private Context context;

    TabletopDetailsFragmentPresenterImpl(TabletopDetailsFragmentView detailsFragmentView, Context context)
    {
        this.detailsFragment = detailsFragmentView;
        this.context = context;
    }

    @Override
    public void onResume()
    {

    }

    @Override
    public void onDestroy()
    {
        detailsFragment = null;
    }
}