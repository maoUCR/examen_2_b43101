package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;

import java.util.List;

public interface TabletopInteractor {
    interface OnFinishedListener {
        void onFinished(List<Tabletop> games);
    }
    void getItems(OnFinishedListener listener, final Context context);
}
