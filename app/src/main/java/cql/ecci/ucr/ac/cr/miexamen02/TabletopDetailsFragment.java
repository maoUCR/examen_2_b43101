package cql.ecci.ucr.ac.cr.miexamen02;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TabletopDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabletopDetailsFragment extends Fragment implements TabletopDetailsFragmentView {
    private View v;
    private Tabletop tabletop;
    private TabletopDetailsFragmentPresenter fragmentPresenter;


    public TabletopDetailsFragment() {
    }

    public static TabletopDetailsFragment newInstance() {
        TabletopDetailsFragment fragment = new TabletopDetailsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_details, container, false);
        Bundle bundle = getActivity().getIntent().getExtras();

        fragmentPresenter = new TabletopDetailsFragmentPresenterImpl(this, getContext());

        if (bundle != null) {
            tabletop = bundle.getParcelable("TABLETOP");

            TextView nombre = (TextView) v.findViewById(R.id.nombre);
            TextView id = (TextView) v.findViewById(R.id.identificacion);
            TextView publisher = (TextView) v.findViewById(R.id.publisher);
            TextView year = (TextView) v.findViewById(R.id.year);

            nombre.setText("Nombre: " + tabletop.getNombre());
            id.setText("ID: " + tabletop.getIdentificacion());
            publisher.setText("Carnet: " + tabletop.getPublisher());
            year.setText("Carnet: " + tabletop.getYear());
        }
        return v;
    }

    @Override
    public void onResume()
    {
        fragmentPresenter.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy()
    {
        fragmentPresenter.onDestroy();
        super.onDestroy();
    }
}