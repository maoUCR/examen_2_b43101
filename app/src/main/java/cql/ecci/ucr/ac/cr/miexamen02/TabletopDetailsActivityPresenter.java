package cql.ecci.ucr.ac.cr.miexamen02;

public interface TabletopDetailsActivityPresenter {
    void onResume();

    void onDestroy();
}
