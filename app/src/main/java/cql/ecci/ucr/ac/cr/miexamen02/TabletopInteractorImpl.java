package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;

import java.util.List;

public class TabletopInteractorImpl implements TabletopInteractor {
    private ItemsRepository mItemsRepository;

    @Override
    public void getItems(OnFinishedListener listener, Context context) {
        List<Tabletop> games = null;
        mItemsRepository = new ItemsRepositoryImpl();

        try {
            games = mItemsRepository.obtainItems(context);
        } catch (CantRetrieveItemsException e) {
            e.printStackTrace();
        }
        listener.onFinished(games);
    }
}
