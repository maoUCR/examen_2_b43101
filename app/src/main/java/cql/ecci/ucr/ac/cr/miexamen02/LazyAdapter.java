package cql.ecci.ucr.ac.cr.miexamen02;

public interface LazyAdapter {
    Object getItem(int position);
}
