package cql.ecci.ucr.ac.cr.miexamen02;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class LazyAdapterImpl extends BaseAdapter implements LazyAdapter {
    private Context context;
    private List<Tabletop> games;

    public LazyAdapterImpl(Context context, List<Tabletop> list)
    {
        this.context = context;
        this.games = list;
    }

    @Override
    public int getCount()
    {
        return games.size();
    }

    @Override
    public Object getItem(int position)
    {
        return games.get(position);
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.custom_list_item, parent, false);
        Tabletop tabletop = games.get(position);

        TextView nombre = (TextView) rowView.findViewById(R.id.gameName);
        TextView publisher = (TextView)rowView.findViewById(R.id.gamePublisher);

        nombre.setText(tabletop.getNombre());
        publisher.setText(tabletop.getPublisher());

        return rowView;
    }

}
