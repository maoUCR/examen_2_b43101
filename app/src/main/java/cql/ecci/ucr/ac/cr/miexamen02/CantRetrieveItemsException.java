package cql.ecci.ucr.ac.cr.miexamen02;

public class CantRetrieveItemsException extends Exception {
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}